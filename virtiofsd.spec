Summary:        Virtio-fs vhost-user device daemon (Rust version)
Name:           virtiofsd
Version:        1.4.0
Release:        10%{?dist}
License:        ASL 2.0 and BSD
URL:            https://gitlab.com/virtio-fs/virtiofsd
Source0:        https://gitlab.com/virtio-fs/virtiofsd/-/archive/v%{version}/%{name}-v%{version}.tar.gz
Source1:        virtiofsd-1.4.0-vendor.tar.bz2

Patch3000:	0001-virtiofsd-v1.4.0-add-loongarch64-support.patch

ExclusiveArch:  %{rust_arches}

BuildRequires:  rust cargo libcap-ng-devel libseccomp-devel rust-toolset
Requires:       qemu-common
Provides:       vhostuser-backend(fs)

Conflicts:      qemu-virtiofsd

%description
A virtio-fs vhost-user device daemon written in Rust.

%prep
%autosetup -n %{name}-v%{version} -p1

%cargo_prep -V 1


%build
%cargo_build



%install
mkdir -p %{buildroot}%{_libexecdir}
install -D -p -m 0755 target/release/virtiofsd %{buildroot}%{_libexecdir}/virtiofsd
install -D -p -m 0644 50-qemu-virtiofsd.json %{buildroot}%{_datadir}/qemu/vhost-user/50-qemu-virtiofsd.json



%files
%license LICENSE-APACHE LICENSE-BSD-3-Clause
%doc README.md
%{_libexecdir}/virtiofsd
%{_datadir}/qemu/vhost-user/50-qemu-virtiofsd.json



%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.4.0-10
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.4.0-9
- Rebuilt for loongarch release

* Tue Jul 16 2024 Huang Yang <huangyang@loongson.cn> - 1.4.0-8
- [Type] other
- [DESC] add support for loongarch64

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.4.0-7
- Rebuilt for OpenCloudOS Stream 23.09

* Tue Jul 18 2023 Wang Guodong <gordonwwang@tencent.com> - 1.4.0-6
- Rebuilt for rust 1.69.0

* Tue May 16 2023 cunshunxia <cunshunxia@tencent.com> - 1.4.0-5
- add conflicts between qemu-virtiofsd.

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.4.0-4
- Rebuilt for OpenCloudOS Stream 23.05

* Mon Apr 03 2023 cunshunxia <cunshunxia@tencent.com> - 1.4.0-3
- use rust-toolset instead of rust-packaging.

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.4.0-2
- Rebuilt for OpenCloudOS Stream 23

* Tue Nov 22 2022 cunshunxia <cunshunxia@tencent.com> - 1.4.0-1
- initial build
